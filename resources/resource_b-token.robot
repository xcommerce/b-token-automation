*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library             Selenium2Library

*** Keywords ***

Open Browser To Homepage
    Open Browser                          ${HOMEPAGE_URL}   ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed                    ${DELAY}

Open Login Page
    Wait Until Element Is Visible         ${login_buuton}   timeout=15
    Click Element                         ${login_buuton}

    Wait Until Element Is Visible         ${login_popup}   timeout=15

Complete With Valid Dates And Submit User Login
    Insert Valid Email
    Insert Valid Password
    Submit User Login And Check Location Page

Insert Valid Email
    Wait Until Element Is Visible         css=input[name="email"]
    Input Text                            email           ${VALID_USER}

Insert Valid Password
    Wait Until Element Is Visible         css=input[name="password"]
    Input Text                            password          ${VALID_PASSWORD}

Submit User Login And Check Location Page
    Click Element                         css=input[name="btn_login"]
    Wait Until Element Is Not Visible     ${login_popup}    timeout=15

Go To Home Page
    Go to                                 ${HOMEPAGE_URL}
    Location Should Be                    ${HOMEPAGE_URL}

Scroll Page To Location
    [Arguments]                           ${x_location}    ${y_location}
    Execute JavaScript                    window.scroll(${x_location},${y_location})

Select Product And Add To Cart From Homepage
    Scroll Page To Location               0  1000

    Add the second product From homepage

    Close Popup Successfully Add To Cart

Add the second product From homepage
    Wait Until Element Is Visible         ${button_add_to_cart_homepage}        timeout=15
    Click Element                         ${button_add_to_cart_homepage}

Close Popup Successfully Add To Cart
    Wait Until Element Is Visible         css=a[class="go_shop cancel_popup_btn cancel_popup_btn--full"]        timeout=15
    Click Element                         css=a[class="go_shop cancel_popup_btn cancel_popup_btn--full"]

    Wait Until Element Is Not Visible     ${successfully_add_to_cart_popup}        timeout=15

Add Product To Cart
    Out Of Stock Condition

    ${don't_display_button}               ${value}=       run keyword and ignore error   Element Should Not Be Visible       ${add_to_cart_details_page}       timeout=15
    Run Keyword If	                      '${don't_display_button}' == 'PASS'    Order Now

    ${don't_display_button}               ${value}=       run keyword and ignore error   Element Should Be Visible       ${add_to_cart_details_page}       timeout=15
    Run Keyword If	                      '${don't_display_button}' == 'PASS'    Add to cart button selection

Add to cart button selection
    Click Element                         ${add_to_cart_details_page}

    Back To Browse

Out Of Stock Condition
    ${out_of_stock_condition}             ${value}=       run keyword and ignore error   Element Should Be Visible       css=div[class="temp_out_of_stock"]
    Run Keyword If	                      '${out_of_stock_condition}' == 'PASS'    Select A Random Product From Browse

Order Now
    Wait Until Element Is Visible         css=a[class="customize_btn link_wit_qty"]        timeout=15
    Click Element                         css=a[class="customize_btn link_wit_qty"]

    ${go_to_step}                         ${value}=       run keyword and ignore error   Element Should Be Visible       ${go_to_next_step}        timeout=15
    Run Keyword If	                      '${go_to_step}' == 'PASS'    Go To Step

    Wait Until Element Is Visible         css=a[class="add"]       timeout=15
    Click Element                         css=a[class="add"]

    Back To Browse

Go To Step
    Click Element                         ${go_to_next_step}

    Wait Until Element Is Visible         ${go_to_next_step}        timeout=15
    Click Element                         ${go_to_next_step}

    ${go_to_step1}                        ${value}=       run keyword and ignore error   Element Should Be Visible       ${go_to_next_step}        timeout=15
    Run Keyword If	                      '${go_to_step1}' == 'PASS'    Click Element                         ${go_to_next_step}

    ${go_to_step2}                        ${value}=       run keyword and ignore error   Element Should Be Visible       ${go_to_next_step}        timeout=15
    Run Keyword If	                      '${go_to_step2}' == 'PASS'    Click Element                         ${go_to_next_step}

Back To Browse
    Wait Until Element Is Visible         css=a[class="customize_btn black"]       timeout=15
    Click Element                         css=a[class="customize_btn black"]

    Wait Until Element Is Visible         css=div[id="prod_boxes"]       timeout=15

Open Browse Categories From Header
    Scroll Page To Location               0  0

    Wait Until Element Is Visible         xpath=//*[@id="wrap"]/div[1]/div/div/div/div[2]/div/div[2]/div[2]/ul/li[4]/a       timeout=15
    Click Element                         xpath=//*[@id="wrap"]/div[1]/div/div/div/div[2]/div/div[2]/div[2]/ul/li[4]/a

    ${browse_page}                        ${value}=       run keyword and ignore error   Element Should Not Be Visible         css=div[class="col-tn-12 col-xs-12 col-sm-8 col-md-9"]       timeout=15
    Run Keyword If	                      '${browse_page}' == 'PASS'    Wait Until Element Is Visible         css=div[id="prod_boxes"]       timeout=15

    Wait Until Element Is Visible         css=div[id="prod_boxes"]    timeout=15

Add To Cart A Product From Browse
    ${category_random}=                 Evaluate    random.randint(782, 783)  random

    Wait Until Element Is Visible         css=#btn_${category_random}       timeout=15
    Click Element                         css=#btn_${category_random}

    Close Popup Successfully Add To Cart

Select A Random Product From Browse
    Wait Until Element Is Visible         xpath=//*[@id="wrap"]/div[1]/div/div/div/div[2]/div/div[2]/div[2]/ul/li[2]/a       timeout=15
    Click Element                         xpath=//*[@id="wrap"]/div[1]/div/div/div/div[2]/div/div[2]/div[2]/ul/li[2]/a

    Select Random A Category
    Wait Until Element Is Visible         css=div[id="prod_boxes"]    timeout=15

    ${products_random}=                   Evaluate    random.randint(1, 5)  random

    Wait Until Element Is Visible         xpath=//*[@id="prod_boxes"]/div[${products_random}]     timeout=15
#    sleep                                 2scss=#prod_boxes > div:nth-child(${products_random})
    Click Element                         xpath=//*[@id="prod_boxes"]/div[${products_random}]

    Wait Until Element Is Visible         xpath=//*[@id="wrap"]/div[2]/div[1]/div/div[2]/div[1]/div[2]     timeout=15

Select Random A Category
    Wait Until Element Is Visible         xpath=//*[@id="prod_boxes"]       timeout=15

    ${category_random}=                   Evaluate    random.randint(1, 12)  random

    Wait Until Element Is Visible         css=#prod_boxes > div:nth-child(${category_random})    timeout=15
    Click Element                         css=#prod_boxes > div:nth-child(${category_random})

Select Name Of The Product And Search It Use Autocomplete
    Open Browse Categories From Header

#save the name of the product
    ${products_random}=                   Evaluate    random.randint(1, 5)  random

    Wait Until Element Is Visible         css=#prod_boxes > div:nth-child(${products_random}) > div > div.product__info > div.product_title > h2     timeout=15

    ${PRODUCT_NAME}=                      Get Text    css=#prod_boxes > div:nth-child(${products_random}) > div > div.product__info > div.product_title > h2

#Use the search field
    Wait Until Element Is Visible         ${search_field}    timeout=15
    Input Text                            ${search_field}     ${PRODUCT_NAME}

    Wait Until Element Is Visible         ${search_autocomplete}    timeout=15
    Click Element                         ${search_autocomplete}

Go To Cart
    Wait Until Element Is Visible       ${popup_go_to_cart}        timeout=15
    Click Element                       ${popup_go_to_cart}

    Wait Until Element Is Visible       ${button_go_to_cart}        timeout=15
    Click Element                       ${button_go_to_cart}

    Wait Until Element Is Visible       css=div[id="tab_shopping_cart"]        timeout=15

Check The Price For Products
    ${get_price1}=                      Get Text    xpath=//*[@id="tab_shopping_cart"]/div[3]/div/div/div[1]/div[2]/div/div[1]/div
    ${get_price2}=                      Get Text    xpath=//*[@id="tab_shopping_cart"]/div[4]/div/div/div[1]/div[2]/div/div[1]/div
    ${get_price3}=                      Get Text    xpath=//*[@id="tab_shopping_cart"]/div[5]/div/div/div[1]/div[2]/div/div[1]/div
    ${get_price4}=                      Get Text    xpath=//*[@id="tab_shopping_cart"]/div[6]/div/div/div[1]/div[2]/div/div[1]/div

# Product price is compared with zero value:
     Should Not Be Equal                ${get_price1}   ${get_price0}
     Should Not Be Equal                ${get_price2}   ${get_price0}
     Should Not Be Equal                ${get_price3}   ${get_price0}
     Should Not Be Equal                ${get_price4}   ${get_price0}

Submit The Terms And Conditions
    Scroll Page To Location             0  10000

    Wait Until Element Is Visible       ${submit_terms}        timeout=15
    Click Element                       ${submit_terms}

    Checkbox Should Be Selected         ${submit_terms}
    sleep           2s

Remove The Cart
    Wait Until Element Is Visible       ${remove_4}        timeout=15
    Click Element                       ${remove_4}

     Scroll Page To Location             0  500
    sleep           2s
    Wait Until Element Is Visible       ${remove_3}        timeout=15
    Click Element                       ${remove_3}

    sleep           2s
    Wait Until Element Is Visible       ${remove_2}        timeout=15
    Click Element                       ${remove_2}

    sleep           2s
    Wait Until Element Is Visible       ${remove_1}        timeout=15
    Click Element                       ${remove_1}

    Wait Until Element Is Visible       css=div[class="banner"]        timeout=15


